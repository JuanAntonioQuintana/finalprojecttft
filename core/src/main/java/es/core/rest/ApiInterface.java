package es.core.rest;

import es.core.calendarTools.model.ScheduleList;
import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiInterface {

    @GET("/calendar/v3/calendars/jkoo400do26d7tp94slrholeog@group.calendar.google.com/events?key=AIzaSyDTYw05UdS4G3dfQ9QHQBLBOq1MXLzUrMw")
    Call<ScheduleList> getMyJSON();
}


