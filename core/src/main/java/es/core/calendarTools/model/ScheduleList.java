package es.core.calendarTools.model;

import com.google.gson.annotations.SerializedName;
import java.util.List;

public class ScheduleList {
    @SerializedName("items")
    private List<Schedule> schedules;

    public List<Schedule> getSchedule() {
        return schedules;
    }
}
