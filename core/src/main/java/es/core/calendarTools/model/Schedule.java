package es.core.calendarTools.model;

import com.google.gson.annotations.SerializedName;

public class Schedule {

    @SerializedName("summary")
    private String summary;
    @SerializedName("location")
    private String location;
    @SerializedName("start")
    private DateTime startDate;
    @SerializedName("end")
    private DateTime endDate;
    @SerializedName("description")
    private String description;

    public String getSummary(){
        return summary;
    }

    public String getLocation(){
        return location;
    }

    public String getStartDate(){
        return startDate.getDateTime();
    }

    public String getEndDate(){
        return endDate.getDateTime();
    }

    public String getDescription(){
        return description;
    }
}