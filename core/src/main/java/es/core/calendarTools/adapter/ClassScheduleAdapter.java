package es.core.calendarTools.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import es.core.R;
import es.core.calendarTools.model.Schedule;

public class ClassScheduleAdapter extends RecyclerView.Adapter<ClassScheduleAdapter.ScheduleViewHolder>{

    private List<Schedule> schedules;
    private int rowLayout;
    private Context context;


    static class ScheduleViewHolder extends RecyclerView.ViewHolder {
        LinearLayout itemLayout;
        TextView hour;
        TextView subject;

        ScheduleViewHolder(View v) {
            super(v);
            itemLayout = (LinearLayout) v.findViewById(R.id.item_Layout);
            hour = (TextView) v.findViewById(R.id.textItem1);
            subject = (TextView) v.findViewById(R.id.textItem2);
        }
    }

    public ClassScheduleAdapter(List<Schedule> schedules, int rowLayout, Context context) {
        this.schedules = schedules;
        this.rowLayout = rowLayout;
        this.context = context;
    }

    @Override
    public ClassScheduleAdapter.ScheduleViewHolder onCreateViewHolder(ViewGroup parent,
                                                                 int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new ClassScheduleAdapter.ScheduleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ClassScheduleAdapter.ScheduleViewHolder holder, final int position) {
        holder.subject.setText(schedules.get(position).getSummary());
        try {
            holder.hour.setText(changeFormatDate(schedules.get(position).getStartDate()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private String changeFormatDate(String date) throws ParseException {
        SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", new Locale("es", "ES"));
        SimpleDateFormat outFormat = new SimpleDateFormat("HH:mm", new Locale("es", "ES"));
        return outFormat.format(inFormat.parse(date));
    }

    @Override
    public int getItemCount() {
        if(schedules == null){
            return 0;
        }
        return schedules.size();
    }
}
