package es.core.view.ui;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.text.ParseException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import es.core.CommandCalendarEvent;
import es.core.R;
import es.core.calendarTools.adapter.SubjectNowScheduleAdapter;
import es.core.calendarTools.model.Schedule;
import es.core.calendarTools.model.ScheduleList;
import es.core.interfaces.FragmentReattachListener;
import es.core.rest.ApiClient;
import es.core.rest.ApiInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SubjectNow extends Fragment implements FragmentReattachListener {

    private static final String LOGTAG = SubjectNow.class.getSimpleName();
    private CommandCalendarEvent commandCalendarEvent;
    private ProgressDialog progressDialog;
    private List<Schedule> schedules;
    private List<Schedule> schedulesList;
    private RecyclerView recyclerView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_subject_now_data, container, false);

        commandCalendarEvent = new CommandCalendarEvent(view, getActivity());
        recyclerView = callRecyclerView();

        progressDialog = callProgressDialog();

        callCalendarApi();

        return view;
    }

    private RecyclerView callRecyclerView(){
        return commandCalendarEvent.initializeRecyclerView();
    }
    private ProgressDialog callProgressDialog(){
        return commandCalendarEvent.getProgressDialog();
    }

    private void callCalendarApi(){
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ScheduleList> call = apiService.getMyJSON();

        call.enqueue(new Callback<ScheduleList>() {
            @Override
            public void onResponse(Call<ScheduleList> call, Response<ScheduleList> response) {

                progressDialog.dismiss();
                schedules = response.body().getSchedule();

                repeat();

                try {
                    schedulesList = commandCalendarEvent.getSubjectNowResult(schedules);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                recyclerView.setAdapter(new SubjectNowScheduleAdapter(schedulesList, R.layout.fragment_items, getActivity()));

            }

            @Override
            public void onFailure(Call<ScheduleList> call, Throwable t) {
                Log.e(LOGTAG, t.toString());
            }
        });
    }

    @Override
    public void reAttach() {
        FragmentManager fragment = getFragmentManager();
        if(fragment != null){
            fragment.beginTransaction()
                    .detach(this)
                    .attach(this)
                    .commit();
        }
    }

    public void repeat(){
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
                           @Override
                           public void run() {
                               reAttach();

                           }

                       },

                60000);
    }
}
