package es.core.interfaces;

public interface RepositoryObserver {
    void onUserDataChanged(String token);
}
