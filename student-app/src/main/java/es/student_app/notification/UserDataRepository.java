package es.student_app.notification;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.EditText;


import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.methods.HttpGet;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.entity.StringEntity;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import cz.msebera.android.httpclient.util.EntityUtils;
import es.core.interfaces.PubNubMessage;
import es.core.interfaces.RepositoryObserver;
import es.core.interfaces.Subject;
import es.core.calendarTools.model.Schedule;
import es.core.calendarTools.model.ScheduleList;
import es.core.rest.ApiClient;
import es.core.rest.ApiInterface;
import es.student_app.view.ui.Profile;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserDataRepository implements Subject {

    private long time;
    private String token;
    private String subject = "";
    private String subjectNow = "";
    private String location = "";
    private String start = "";
    private String end = "";
    private int timeEnable = 0;
    private String description = "";
    private static final String TAG = UserDataRepository.class.getSimpleName();
    private List<Schedule> schedules = new ArrayList<>();
    private ArrayList<RepositoryObserver> mObservers;
    private static UserDataRepository INSTANCE = null;
    private final Handler handler = new Handler();
    private String id;
    private String name;
    private String email;
    private String dni;
    private String event = "";
    private SharedPreferences preferences;



    private UserDataRepository(Context context) {
        mObservers = new ArrayList<>();
        updateData();
        receiveSignal(context);
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    private void getDataFromCalendar(final String token){
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ScheduleList> call = apiService.getMyJSON();

        call.enqueue(new Callback<ScheduleList>() {
            @Override
            public void onResponse(Call<ScheduleList> call, Response<ScheduleList> response) {
                schedules = response.body().getSchedule();
                for (int i = 0; i < schedules.size(); i++) {
                    try {
                        if(changeFormatDate(schedules.get(i).getStartDate()).equals(getDate()) && (changeFormatHour(schedules.get(i).getStartDate()).compareTo(getHour()) < 0 ||
                                changeFormatHour(schedules.get(i).getStartDate()).compareTo(getHour()) == 0) &&
                                (changeFormatHour(schedules.get(i).getEndDate()).compareTo(getHour()) > 0) && token.equals(schedules.get(i).getDescription())){
                            initialize(schedules.get(i).getDescription(), schedules.get(i).getLocation(), schedules.get(i).getSummary(), schedules.get(i).getStartDate(), schedules.get(i).getEndDate());
                            break;
                        } else{
                            initialize("", "", "", "", "");
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }


            }

            @Override
            public void onFailure(Call<ScheduleList> call, Throwable t) {
                Log.e(TAG, t.toString());
            }
        });
    }

    private void initialize(String description, String location, String subject, String start, String end){
        this.description = description;
        this.location = location;
        this.subject = subject;
        this.start = start;
        this.end = end;
    }

    private void receiveSignal(Context context) {

        BroadcastReceiver mReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String text = intent.getStringExtra("token");

                getDataFromCalendar(text);
                if (timeEnable != 1 && !start.equals("") && !end.equals("") && !subject.equals("") &&
                        !TextUtils.isEmpty(preferences.getString("name", ""))
                        && !TextUtils.isEmpty(preferences.getString("dni", ""))) {

                    id = Settings.Secure.getString(context.getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
                    subjectNow = subject;
                    name = preferences.getString("name", "");
                    dni = preferences.getString("dni", "");
                    email = preferences.getString("email", "");

                    try {
                        if(!userExist()){
                            createUser();
                        }
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    storeRecord("input");
                    timeEnable = 1;
                }

                if (timeEnable == 1 && !subject.equals(subjectNow)) {
                    storeRecord("input");
                    timeEnable = 0;
                }
                configureProfile(text);
                time = 0;
            }
        };
        context.registerReceiver(mReceiver, new IntentFilter("get token"));
    }

    private void createUser() {
        User user = new User();
        user.execute(id, name, dni, email, "1");
    }

    private boolean userExist() throws ExecutionException, InterruptedException {
        Customer customer = new Customer();
        return customer.execute(id).get();
    }

    private void storeRecord(String event){
        Record record = new Record();
        String startDateTime = null;
        try {
            startDateTime = getCalendarHour(start);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String endDateTime = null;
        try {
            endDateTime = getCalendarHour(end);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        record.execute(id,
                subject,
                String.valueOf(System.currentTimeMillis()),
                location,
                getDate(),
                startDateTime,
                endDateTime,
                event
        );
    }



    private void configureProfile(String token){
        if(subject.equals("")){
            setUserData("Lugar");
        } else {
            setUserData(token);
        }
    }

    private void updateData() {
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        time += 1000;

                        if (time == 8000) {
                            if(timeEnable == 1){
                                storeRecord("output");
                                timeEnable = 0;
                            }

                            setUserData("Lugar");
                        }

                        Log.d("TimerExample", "Going for... " + time);
                    }
                });
            }
        }, 1000, 1000);

    }

    public static UserDataRepository getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new UserDataRepository(context);
        }
        return INSTANCE;
    }

    @Override
    public void registerObserver(RepositoryObserver repositoryObserver) {
        if(!mObservers.contains(repositoryObserver)) {
            mObservers.add(repositoryObserver);
        }
    }

    @Override
    public void removeObserver(RepositoryObserver repositoryObserver) {
        if(mObservers.contains(repositoryObserver)) {
            mObservers.remove(repositoryObserver);
        }
    }

    @Override
    public void notifyObservers() {
        for (RepositoryObserver observer: mObservers) {
            observer.onUserDataChanged(token);
        }
    }

    private void setUserData(String token) {
        this.token = token;
        notifyObservers();
    }

    private String changeFormatDate(String date) throws ParseException {
        SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", new Locale("es","ES"));
        SimpleDateFormat outFormat = new SimpleDateFormat("dd-MM-yyyy", new Locale("es","ES"));
        return outFormat.format(inFormat.parse(date));
    }

    private Date changeFormatHour(String date) throws ParseException {
        SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", new Locale("es","ES"));
        SimpleDateFormat outFormat = new SimpleDateFormat("HH:mm", new Locale("es","ES"));
        String hourSchedule = outFormat.format(inFormat.parse(date));
        return outFormat.parse(hourSchedule);
    }

    private Date getHour() throws ParseException {
        DateFormat dateFormat = new SimpleDateFormat("HH:mm", new Locale("es","ES"));
        Date newDate = new Date();
        String date = dateFormat.format(newDate);
        return dateFormat.parse(date);
    }

    private String getDate(){
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", new Locale("es","ES"));
        Date newDate = new Date();
        return dateFormat.format(newDate);
    }

    private String getCalendarHour(String date) throws ParseException {
        SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", new Locale("es","ES"));
        SimpleDateFormat outFormat = new SimpleDateFormat("HH:mm", new Locale("es","ES"));
        return outFormat.format(inFormat.parse(date));
    }

    private class Record extends AsyncTask<String,Integer,Boolean> {

        protected Boolean doInBackground(String... params) {

            boolean resul = true;

            HttpClient httpClient = new DefaultHttpClient();

            HttpPost post = new HttpPost("https://project-final.herokuapp.com/api/records");
            post.setHeader("content-type", "application/json");
            try
            {
                JSONObject dato = new JSONObject();

                dato.put("idAndroid", params[0]);
                dato.put("subject", params[1]);
                dato.put("time", params[2]);
                dato.put("classroom", params[3]);
                dato.put("date", params[4]);
                dato.put("start_date_time", params[5]);
                dato.put("end_date_time", params[6]);
                dato.put("event", params[7]);

                StringEntity entity = new StringEntity(dato.toString());
                post.setEntity(entity);
                HttpResponse resp = httpClient.execute(post);
                String respStr = EntityUtils.toString(resp.getEntity());
                if(!respStr.equals("true"))
                    resul = false;
            }
            catch(Exception ex)
            {
                Log.e("ServicioRest","Error!", ex);
                resul = false;
            }

            return resul;
        }

        protected void onPostExecute(Boolean result) {
            if (result)
            {
                System.out.println("Insert in database");
            }
        }
    }


    private class User extends AsyncTask<String,Integer,Boolean> {

        protected Boolean doInBackground(String... params) {

            boolean resul = true;

            HttpClient httpClient = new DefaultHttpClient();

            HttpPost post = new HttpPost("https://project-final.herokuapp.com/api/users");
            post.setHeader("content-type", "application/json");

            try
            {
                JSONObject dato = new JSONObject();
                dato.put("idAndroid", params[0]);
                dato.put("name", params[1]);
                dato.put("dni", params[2]);
                dato.put("email", params[3]);
                dato.put("status", params[4]);

                StringEntity entity = new StringEntity(dato.toString());
                post.setEntity(entity);

                HttpResponse resp = httpClient.execute(post);

                String respStr = EntityUtils.toString(resp.getEntity());

                if(!respStr.equals("true"))
                    resul = false;
            }
            catch(Exception ex)
            {
                Log.e("ServicioRest","Error!", ex);
                resul = false;
            }

            return resul;
        }

        protected void onPostExecute(Boolean result) {
            if (result)
            {
                System.out.println("Insert in database");
            }
        }
    }


    private class Customer extends AsyncTask<String,Integer,Boolean> {

        private String[] customers;

        protected Boolean doInBackground(String... params) {

            boolean resul = true;

            String id = params[0];

            HttpClient httpClient = new DefaultHttpClient();

            HttpGet del =
                    new HttpGet("https://project-final.herokuapp.com/api/records/index/"+ id);

            del.setHeader("content-type", "application/json");

            try
            {
                HttpResponse resp = httpClient.execute(del);
                String respStr = EntityUtils.toString(resp.getEntity());

                JSONObject responseJSON = new JSONObject(respStr);

                JSONArray users = responseJSON.getJSONArray("record");

                if(users.length() > 0){
                    return resul;
                }
            }
            catch(Exception ex)
            {
                Log.e("ServicioRest","Error!", ex);
                resul = false;
            }

            return resul;
        }

        protected void onPostExecute(Boolean result) {
            if (result)
            {

            }
        }
    }
}
