package es.student_app.view.ui;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;
import com.google.gson.JsonElement;
import com.pubnub.api.PNConfiguration;
import com.pubnub.api.PubNub;
import com.pubnub.api.callbacks.PNCallback;
import com.pubnub.api.models.consumer.PNStatus;
import com.pubnub.api.models.consumer.history.PNHistoryItemResult;
import com.pubnub.api.models.consumer.history.PNHistoryResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.methods.HttpGet;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import cz.msebera.android.httpclient.util.EntityUtils;
import es.student_app.R;

public class AttendanceRecord extends Fragment {

    private View view;
    private Button calendar_start;
    private Button calendar_end;
    private Spinner subjects_spinner;
    private ListView list;
    private List<String> subjects = new ArrayList<>();
    private List<String> attendance = new ArrayList<>();;
    private long startTime, endTime;
    private String androidId;
    private String startDate;
    private String endDate;
    private Calendar myCalendar = Calendar.getInstance();

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_attendance_record, container, false);

        this.androidId = Settings.Secure.getString(getActivity().getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);

        initializeElements();
        try {
            configureCalendar();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return view;
    }

    private void initializeElements() {
        calendar_start = (Button) view.findViewById(R.id.calendar_start_spinner);
        calendar_end = (Button) view.findViewById(R.id.calendar_end_spinner);
        subjects_spinner = (Spinner) view.findViewById(R.id.subject_spinner);
        list = (ListView) view.findViewById(R.id.lstOptions);
    }

    private String configureDate(){
        String myFormat = "dd-MM-yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        return sdf.format(myCalendar.getTime());

    }

    private class Student extends AsyncTask<String,Integer,Boolean> {

        private ArrayList<JSONObject> studentRecords = new ArrayList<>();

        protected Boolean doInBackground(String... params) {

            boolean resul = true;

            String id = params[0];

            HttpClient httpClient = new DefaultHttpClient();

            HttpGet del =
                    new HttpGet("https://project-final.herokuapp.com/api/records/index/"+ id);

            del.setHeader("content-type", "application/json");

            try
            {
                HttpResponse resp = httpClient.execute(del);
                String respStr = EntityUtils.toString(resp.getEntity());

                JSONObject responseJSON = new JSONObject(respStr);

                JSONArray records = responseJSON.getJSONArray("record");

                for (int i = 0; i < records.length(); i++) {
                    JSONObject object = records.getJSONObject(i);
                    studentRecords.add(object);
                }

                getSubjects(studentRecords, params[1], params[2]);


            }
            catch(Exception ex)
            {
                Log.e("ServicioRest","Error!", ex);
                resul = false;
            }

            return resul;
        }

        protected void onPostExecute(Boolean result) {
            if (result)
            {
            }
        }
    }

    public void getSubjects(ArrayList<JSONObject> studentRecords, final String startDate, final String endDate) throws JSONException {
        final ArrayList<String> finalSubjects = new ArrayList<>();

        for (int i = 0; i < studentRecords.size(); i++) {
            if(!subjects.contains(studentRecords.get(i).getString("subject"))){
                subjects.add(studentRecords.get(i).getString("subject"));
            }
        }

        getActivity().runOnUiThread(new Runnable()
        {
            public void run()
            {

                for (int i = 0; i < subjects.size(); i++) {
                    try {
                        Subject container = new Subject();
                        if(container.execute(androidId, startDate, endDate, subjects.get(i)).get()){
                            finalSubjects.add(subjects.get(i));
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    }
                }

                if(finalSubjects.size() == 0){
                    attendance = new ArrayList<String>();
                    configureAdapter(attendance);
                }

                insertSubjectsIntoSpinner(finalSubjects);
            }
        });
    }

    public void insertSubjectsIntoSpinner(List<String> subjects){

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_spinner_item, subjects);

        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        subjects_spinner.setAdapter(dataAdapter);

        subjects_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                Toast.makeText(parent.getContext(), "Selected!", Toast.LENGTH_SHORT).show();
                String subjectSelected = parent.getItemAtPosition(position).toString();
                attendance = new ArrayList<String>();

                Data data = new Data();
                data.execute(androidId, startDate, endDate, subjectSelected);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });
    }

    private void configureCalendar() throws ParseException {

        View.OnClickListener showDatePicker = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final View vv = v;

                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        myCalendar.set(Calendar.YEAR, year);
                        myCalendar.set(Calendar.MONTH, month);
                        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                        if (vv.getId() == R.id.calendar_start_spinner) {
                            startDate = configureDate();
                            calendar_start.setText(startDate);
                        } else {
                            endDate = configureDate();
                            if(endDate.compareTo(startDate) < 1){
                                new AlertDialog.Builder(getActivity())
                                        .setTitle("Fecha errónea")
                                        .setMessage("La fecha introducida es incorrecta")
                                        .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {

                                            }
                                        }).show();
                            }else {
                                calendar_end.setText(endDate);
                            }
                        }

                        if(startDate != null && endDate != null) {
                            Student student = new Student();
                            student.execute(androidId, startDate, endDate);
                        }
                    }
                }, Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
                datePickerDialog.show();
            }
        };
        calendar_start.setOnClickListener(showDatePicker);
        calendar_end.setOnClickListener(showDatePicker);
    }

    public String changeFormatDate(String date) throws ParseException {
        SimpleDateFormat inFormat = new SimpleDateFormat("dd-MM-yyyy", new Locale("es", "ES"));
        SimpleDateFormat outFormat = new SimpleDateFormat("EEE, d MMM yyyy", new Locale("es", "ES"));
        return outFormat.format(inFormat.parse(date));
    }


    private class Data extends AsyncTask<String,Integer,Boolean> {

        private String[] records;

        protected Boolean doInBackground(String... params) {

            boolean resul = true;

            String androidId = params[0];
            String start_date = params[1];
            String end_date = params[2];
            String subject = params[3];

            HttpClient httpClient = new DefaultHttpClient();

            HttpGet del =
                    new HttpGet("https://project-final.herokuapp.com/api/records/index/"+ androidId
                                +"/"+ start_date + "/" + end_date + "/" + subject.replaceAll(" ","%20") );

            del.setHeader("content-type", "application/json");

            try
            {
                HttpResponse resp = httpClient.execute(del);
                String respStr = EntityUtils.toString(resp.getEntity());

                JSONObject responseJSON = new JSONObject(respStr);

                JSONArray elements = responseJSON.getJSONArray("record");

                records = new String[elements.length()];

                for (int i = 0; i < elements.length(); i++) {
                    JSONObject object = elements.getJSONObject(i);
                    subject = object.getString("subject");
                    String time = object.getString("time");
                    String classroom = object.getString("classroom");
                    String date = object.getString("date");
                    String start_date_time = object.getString("start_date_time");
                    String end_date_time = object.getString("end_date_time");
                    attendance.add(changeFormatDate(date) + "  " + start_date_time + "-" + end_date_time + "(" + time + ")" + " " + subject + " " + classroom);
                }

                configureAdapter(attendance);

            }
            catch(Exception ex)
            {
                Log.e("ServicioRest","Error!", ex);
                System.out.println("Problem found");
                resul = false;
            }

            return resul;
        }

        protected void onPostExecute(Boolean result) {
            if (result)
            {

            }
        }
    }

    private void configureAdapter(final List<String> attendance){

        getActivity().runOnUiThread(new Runnable()
        {
            public void run()
            {
                ArrayAdapter<String> adapter =
                        new ArrayAdapter<String>(getActivity(),
                                android.R.layout.simple_list_item_1, attendance);

                list.setAdapter(adapter);
            }
        });

    }

    private class Subject extends AsyncTask<String,Integer,Boolean> {

        private String[] records;

        protected Boolean doInBackground(String... params) {

            boolean resul = true;

            String androidId = params[0];
            String start_date = params[1];
            String end_date = params[2];
            String subject = params[3];

            HttpClient httpClient = new DefaultHttpClient();

            HttpGet del =
                    new HttpGet("https://project-final.herokuapp.com/api/records/index/"+ androidId +"/"+ start_date + "/" + end_date + "/" + subject.replaceAll(" ","%20") );

            del.setHeader("content-type", "application/json");

            try
            {
                HttpResponse resp = httpClient.execute(del);
                String respStr = EntityUtils.toString(resp.getEntity());

                JSONObject responseJSON = new JSONObject(respStr);

                JSONArray elements = responseJSON.getJSONArray("record");

                if (elements.length() > 0){return true;}
                else{
                    resul = false;
                }

            }
            catch(Exception ex)
            {
                Log.e("ServicioRest","Error!", ex);
                resul = false;
            }

            return resul;

        }

        protected void onPostExecute(Boolean result) {
            if (result)
            {

            }
        }
    }

}
