package es.student_app.view.ui;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import es.core.calendarTools.model.Schedule;
import es.core.interfaces.FragmentReattachListener;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import es.core.view.ui.*;
import es.core.R;
import es.core.calendarTools.adapter.ClassScheduleAdapter;
import es.core.calendarTools.model.ScheduleList;
import es.core.rest.ApiClient;
import es.core.rest.ApiInterface;

import es.core.calendarTools.DividerItemDecoration;

public class ClassSchedule extends Fragment implements FragmentReattachListener {

    private static final String TAG = SubjectNow.class.getSimpleName();
    private List<Schedule> schedules;
    private List<Schedule> schedulesList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_class_schedule_data, container, false);
        final RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.schedule_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(
                new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST));
        recyclerView.setItemAnimator(new DefaultItemAnimator());


        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.setMessage(getString(R.string.string_message));
        dialog.show();

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ScheduleList> call = apiService.getMyJSON();

        call.enqueue(new Callback<ScheduleList>() {
            @Override
            public void onResponse(Call<ScheduleList> call, Response<ScheduleList> response) {
                dialog.dismiss();

                schedules = response.body().getSchedule();
                repeat();
                try {
                    schedulesList = getResult(schedules);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                recyclerView.setAdapter(new ClassScheduleAdapter(schedulesList, R.layout.fragment_items, getActivity()));
            }

            @Override
            public void onFailure(Call<ScheduleList> call, Throwable t) {
                Log.e(TAG, t.toString());
            }
        });
        return view;
    }

    private List<Schedule> getResult(List<Schedule> schedules) throws ParseException {

        ArrayList<Schedule> schedulesR = new ArrayList<>();
        System.out.println("Esta estoy aqui");
        for (int i = 0; i < schedules.size(); i++) {
            if(changeFormatDate(schedules.get(i).getStartDate()).equals(getDate()) && schedules.get(i).getLocation() != null){
                if(schedules.get(i).getLocation().equals(Profile.idC)) {
                    System.out.println("Esta entrandooooooo");
                    schedulesR.add(schedules.get(i));
                }
            }

        }
        return schedulesR;
    }

    public String changeFormatDate(String date) throws ParseException {
        SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", new Locale("es", "ES"));
        SimpleDateFormat outFormat = new SimpleDateFormat("dd MMMM yyyy", new Locale("es", "ES"));
        return outFormat.format(inFormat.parse(date));
    }

    public String getDate(){
        DateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy", new Locale("es", "ES"));
        Date newDate = new Date();
        return dateFormat.format(newDate);
    }


    @Override
    public void reAttach() {
        FragmentManager f = getFragmentManager();
        if (f != null) {
            f.beginTransaction()
                    .detach(this)
                    .attach(this)
                    .commit();
        }
    }

    public void repeat(){
        Timer t = new Timer();
        t.schedule(new TimerTask() {
                       @Override
                       public void run() {
                           reAttach();

                       }

                   },

                60000);
    }
}

