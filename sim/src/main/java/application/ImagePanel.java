package application;

import model.Image;
import view.ImageDisplay;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;

class ImagePanel extends JPanel implements ImageDisplay {

    private Image image;

    ImagePanel(Image image){
        this.image = image;
    }

    @Override
    protected void paintComponent(Graphics g) {
        g.drawImage((BufferedImage) image.bitMap(), 0, 0, this);
    }

    @Override
    public Image image() {
        return image;
    }

}
