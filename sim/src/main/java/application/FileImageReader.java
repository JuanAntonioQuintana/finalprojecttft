package application;

import model.Image;
import view.ImageReader;
import javax.imageio.ImageIO;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;

class FileImageReader implements ImageReader {

    private final File[] files;
    private static final String[] ImageExtensions={".jpg",".png",".gif"};

    FileImageReader(String path) {
        this(new File(path));
    }

    private FileImageReader(File folder){
        this.files = folder.listFiles(withImageExtension());
    }

    private FilenameFilter withImageExtension() {
        return new FilenameFilter(){
            @Override
            public boolean accept(File dir, String name){
                for (String extension : ImageExtensions) {
                    if(name.endsWith(extension))return true;
                }
                return false;
            }
        };
    }


    @Override
    public Image read() {
        return imageAt(0);
    }

    private Image imageAt(final int index){
        return new Image() {
            @Override
            public Object bitMap() {
                try {
                    return ImageIO.read(files[index]);
                } catch (IOException e) {
                    return null;
                }
            }
        };
    }
}

