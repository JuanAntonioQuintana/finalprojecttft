package view;

import model.Image;

public interface ImageDisplay {

    Image image();
}
